<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dom Comunicação :: Sistema de Gerenciamento de PDVS</title>
    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="styles.css"">
    <link rel="stylesheet" href="css/material.blue-red.min.css">
    <link rel="stylesheet" href="css/mdl-jquery-modal-dialog.css">

    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://code.getmdl.io/1.1.1/material.min.js"></script>
    <style>
        #view-source {
            position: fixed;
            display: block;
            right: 0;
            bottom: 0;
            margin-right: 40px;
            margin-bottom: 40px;
            z-index: 900;
        }
    </style>
</head>
<body>

<div class="dialog-container login_dialog" style="opacity: 1;">
    <div class="mdl-card mdl-shadow--16dp login">
<!--        <img src="--><?//= base_url("includes/images/logo.png") ?><!--">-->
        <p></p>
        <form action="controller/login.php?action=login" method="post">
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="login" name="login" >
                <label class="mdl-textfield__label" for="login">Login</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="password" id="senha" name="senha" >
                <label class="mdl-textfield__label" for="senha">Senha</label>
            </div>

        <p></p>
        <div class="mdl-card__actions dialog-button-bar">
            <input type="submit" value="Entrar" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="positive">
        </div>
        </form>
    </div>
</div>
</body>

<!--    <a href="https://github.com/google/material-design-lite/blob/master/templates/blog/" target="_blank" id="view-source" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--white">View Source</a>-->
<script src="https://code.getmdl.io/1.1.1/material.min.js"></script>
</body>
<script>
    Array.prototype.forEach.call(document.querySelectorAll('.mdl-card__media'), function(el) {
        var link = el.querySelector('a');
        if(!link) {
            return;
        }
        var target = link.getAttribute('href');
        if(!target) {
            return;
        }
        el.addEventListener('click', function() {
            location.href = target;
        });
    });
    $(document).ready(function() {
        $("#add_album").click(function () {
            $.ajax({
                type: "POST",
                url: "controller/cantor.php?action=getCantores",
                dataType: "text",
                cache: false,
                success: function (data) {
                    var dados = JSON.parse(data);
                    var opcoes = "";
                    dados.forEach(function(valor, chave){
                        console.log(valor);
                        console.log(chave);
                        opcoes += '<option value="'+valor.codigo_cantor+'" >'+valor.nome+'</option>';
                    });
                    var form = '<form action="<?= $Cantor->getServer()."controller/album.php?action=insertAlbum" ?>" method="post" class="form_insert_album" enctype="multipart/form-data">'+
                        '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                        '<input class="mdl-textfield__input" type="text" id="titulo_album" name="titulo_album" required>' +
                        '<label class="mdl-textfield__label" for="titulo_album">Título do Álbum</label>' +
                        '</div>' +
                        '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                        '<input class="mdl-textfield__input" type="file" id="capa_album" name="capa_album" required>' +
                        '<label class="mdl-textfield__label" for="capa_album">Capa do Álbum</label>' +
                        '</div>' +
                        '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                        '<input class="mdl-textfield__input" type="text" id="ano_album" name="ano_album" required>' +
                        '<label class="mdl-textfield__label" for="ano_album">Ano do Álbum</label>' +
                        '</div>' +
                        '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                        '<select id="tipo" class="mdl-textfield__input select_tipo" required name="cantor">' +
                        '<option></option>' +
                        opcoes+
                        '</select>'+
                        '<label class="mdl-textfield__label" for="tipo">Cantor</label>' +
                        '</div>' +
                        '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                        '<textarea class="mdl-textfield__input" id="descricao_album" name="descricao_album" required></textarea>' +
                        '<label class="mdl-textfield__label" for="descricao_album">Descrição do Álbum</label>' +
                        '</div>' +
                        '<input type="submit" class="hide"></button>'+
                        '</form>';
                    showDialog({
                        title: 'Adicionando Albúns',
                        text: form,
                        negative: {
                            title: 'Cancelar'
                        },
                        positive: {
                            title: 'Salvar',
                            onClick: function (e) {
                                $(".form_insert_album").find('[type="submit"]').trigger('click');
                            }
                        }
                    });
                    //removendo loading
//                    $(link).children('i').removeClass('rotate').html('mode_edit');

                }
            });
        });
        $(".edit_album").click(function () {
            var id_album = $(this).data('idalbum');
            $.ajax({
                type: "POST",
                url: "<?= $App->getServer() ?>controller/album.php?action=getInfo&id="+id_album,
                dataType: "text",
                cache: false,
                success: function (data) {
                    var dados = JSON.parse(data);
                    var opcoes = "";
                    dados.cantores.forEach(function(valor, chave){
                        opcoes += (dados.album[0].cantor_fk === valor.codigo_cantor) ? '<option selected value="'+valor.codigo_cantor+'" >'+valor.nome+'</option>' : '<option value="'+valor.codigo_cantor+'" >'+valor.nome+'</option>';
                    });
                    var form = '<form action="<?= $Cantor->getServer()."controller/album.php?action=editAlbum&id=" ?>'+dados.album[0].codigo+'" method="post" class="form_insert_album" enctype="multipart/form-data">'+
                        '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                        '<input class="mdl-textfield__input" value="'+dados.album[0].titulo+'" type="text" id="titulo_album" name="titulo_album" required>' +
                        '<label class="mdl-textfield__label" for="titulo_album">Título do Álbum</label>' +
                        '</div>' +
                        '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                        '<input class="mdl-textfield__input" type="file" id="capa_album" name="capa_album">' +
                        '<label class="mdl-textfield__label" for="capa_album">Capa do Álbum</label>' +
                        '</div>' +
                        '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                        '<input class="mdl-textfield__input"  value="'+dados.album[0].ano_lancamento+'" type="text" id="ano_album" name="ano_album" required>' +
                        '<label class="mdl-textfield__label" for="ano_album">Ano do Álbum</label>' +
                        '</div>' +
                        '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                        '<select id="tipo" class="mdl-textfield__input select_tipo" required name="cantor">' +
                        '<option></option>' +
                        opcoes+
                        '</select>'+
                        '<label class="mdl-textfield__label" for="tipo">Cantor</label>' +
                        '</div>' +
                        '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                        '<textarea class="mdl-textfield__input" id="descricao_album" name="descricao_album" required> '+dados.album[0].descricao+'</textarea>' +
                        '<label class="mdl-textfield__label" for="descricao_album">Descrição do Álbum</label>' +
                        '</div>' +
                        '<input type="submit" class="hide"></button>'+
                        '</form>';
                    showDialog({
                        title: 'Alterando Album',
                        text: form,
                        negative: {
                            title: 'Cancelar'
                        },
                        positive: {
                            title: 'Salvar',
                            onClick: function (e) {
                                $(".form_insert_album").find('[type="submit"]').trigger('click');
                            }
                        },
                        cancelable: false
                   });
                }
            });
        });

        $("#add_cantor").click(function () {
            var form = '<form action="<?= $Cantor->getServer()."controller/cantor.php?action=insertCantor" ?>" method="post" class="form_insert_album" enctype="multipart/form-data">'+
                '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                '<input class="mdl-textfield__input" type="text" id="cantor" name="cantor" required>' +
                '<label class="mdl-textfield__label" for="cantor">Cantor(a) ou Banda</label>' +
                '</div>' +
                '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                '<input class="mdl-textfield__input" type="file" id="imagem_cantor" name="imagem_cantor" required>' +
                '<label class="mdl-textfield__label" for="imagem_cantor">Imagem</label>' +
                '</div>' +
                '<input type="submit" class="hide"></button>'+
                '</form>';
            showDialog({
                title: 'Adicionando Cantor(a)/Banda',
                text: form,
                negative: {
                    title: 'Cancelar'
                },
                positive: {
                    title: 'Salvar',
                    onClick: function (e) {
                        $(".form_insert_album").find('[type="submit"]').trigger('click');
                    }
                }
            });
        });

        $("#add_usuario").click(function () {
            var form = '<form action="<?= $App->getServer()."controller/usuario.php?action=insertUsuario" ?>" method="post" class="form_insert_album">'+
                '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                '<input class="mdl-textfield__input" type="text" id="nome" name="nome" required>' +
                '<label class="mdl-textfield__label" for="nome">Nome Usuario</label>' +
                '</div>' +
                '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                '<input class="mdl-textfield__input" type="text" id="login" name="login" required>' +
                '<label class="mdl-textfield__label" for="login">Login do Usuário</label>' +
                '</div>' +
                '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">' +
                '<input class="mdl-textfield__input" type="password" id="senha" name="senha" required>' +
                '<label class="mdl-textfield__label" for="senha">Senha</label>' +
                '</div>' +
                '<input type="submit" class="hide"></button>'+
                '</form>';
            showDialog({
                title: 'Adicionando Usuário',
                text: form,
                negative: {
                    title: 'Cancelar'
                },
                positive: {
                    title: 'Salvar',
                    onClick: function (e) {
                        $(".form_insert_album").find('[type="submit"]').trigger('click');
                    }
                },
                cancelable: false

            });
        });
        $(".pesquisa").on("keyup", function(){
            var dados = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?= $App->getServer() ?>controller/pesquisa.php",
                data: dados,
                dataType: "text",
                cache: false,
                success: function (data) {
                    if (dados == ""){//caso não tenha nada na pesquisa, exibe os albuns normalmente
                        $(".show").fadeIn();
                        $(".searchResult").fadeOut();
                    }
                    var resultado = JSON.parse(data);
                    var cantores = "";
                    var albuns = "";

                    if (resultado.cantores != ""){
                        cantores += '<h3>Cantores</h3><div class="demo-list-action mdl-list">';
                        resultado.cantores.forEach(function(indice, vetor){
                            cantores += '<a href="detalhecantor.php?id='+indice.codigo_cantor+'"><div class="mdl-list__item">' +
                               '<span class="mdl-list__item-primary-content">' +
                                '<img src="images/bandas/'+indice.imagem+'" class="material-icons min_search mdl-list__item-avatar" >' +
                               '<span>'+indice.nome+'</span>' +
                               '</span>' +
                               '</div></a>';
                        });
                        cantores += '</div>';
                    }else{
                        cantores = "";
                    }
                    if (resultado.albums != ""){
                        albuns += '<h3>Albums</h3><div class="demo-list-action mdl-list">';
                        resultado.albums.forEach(function(indice, vetor){
                            albuns+= '<a href="detalhecantor.php?id='+indice.cantor_fk+'"><div class="mdl-list__item">' +
                                '<span class="mdl-list__item-primary-content">' +
                                '<img src="images/albums/'+indice.capa+'" class="material-icons min_search mdl-list__item-avatar" >' +
                                '<span>'+indice.ano_lancamento+' - '+indice.titulo+'</span>' +
                                '</span>'+

                                '</div></a>';
                        });
                        albuns += '</div>';
                    }else{
                        albuns = "";
                    }
                    var html = cantores+albuns;
                    html = (html == "")? '<h3>Nenhum Resultado Encontrado</h3>' : html;
                    if (dados != ""){
                        $(".show").fadeOut();
                        $(".searchResult .details").html(html);
                        $(".searchResult").fadeIn();
                    }
                }
            });
        });
    });
</script>
</html>
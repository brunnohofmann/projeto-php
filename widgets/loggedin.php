<?php
if ($Cantor->isLogged()) {
?>
    <div class="mdl-card something-else mdl-cell mdl-cell--8-col mdl-cell--4-col-desktop">
        <a href="controller/login.php?action=logout" class="logged" >
            <button class="mdl-button mdl-js-ripple-effect mdl-js-button mdl-button--fab mdl-color--accent">
                <i class="material-icons mdl-color-text--white" role="presentation">exit_to_app</i>
                <span class="visuallyhidden">add</span>
            </button>
        </a>
        <div class="mdl-card__media mdl-color--white mdl-color-text--grey-600">
            <img src="images/login.png">
        </div>
        <div class="mdl-card__supporting-text meta meta--fill mdl-color-text--grey-600">
            <div>
                <strong><?= $_SESSION['usuario']['nome'] ?></strong>
            </div>
            <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right mdl-js-ripple-effect" for="menubtn">
                <li class="mdl-menu__item" id="add_usuario">Adicionar Usuário</li>
                <li class="mdl-menu__item" id="add_cantor">Adicionar Cantor</li>
                <li class="mdl-menu__item" id="add_album">Adicionar Album</li>
<!--                <li class="mdl-menu__item">Search</li>-->
            </ul>
            <button id="menubtn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                <i class="material-icons" role="presentation">more_vert</i>
                <span class="visuallyhidden">show menu</span>
            </button>
        </div>
    </div>
<?php
}
?>
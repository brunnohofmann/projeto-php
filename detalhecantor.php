<?php
include_once "widgets/topo.php";
$cantorId = $_GET['id'];
$cantor = $Cantor->getCantorById($cantorId);
//coletando os albuns dos cantores
$Albums = new AlbunsCantor($cantorId);
?>
<body>
    <div class="demo-blog mdl-layout mdl-js-layout has-drawer is-upgraded">
      <main class="mdl-layout__content">
          <div class="demo-back">
              <a class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" href="<?= $App->getServer() ?>" title="go back" role="button">
                  <i class="material-icons" role="presentation">arrow_back</i>
              </a>
          </div>
        <div class="demo-blog__posts mdl-grid">
          <div class="mdl-card mdl-cell <?= ($App->isLogged()) ? "mdl-cell--8-col" : "mdl-cell--12-col" ?>">
            <div class="mdl-card__media mdl-color-text--grey-50" style="background-image: url('images/bandas/<?= $cantor['imagem'] ?>')">
              <h3 class="shadow"><?= $cantor['nome'] ?></h3>
            </div>
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <?php include_once "widgets/pesquisa.php" ?>
<!--              <div>-->
<!--                <strong>The Newist</strong>-->
<!--                <span>2 days ago</span>-->
<!--              </div>-->
            </div>
          </div>
          <?php
          //para incluir o widget com as opções para o usuário logado
          include_once "widgets/loggedin.php";
            ?>
            <div class="searchResult mdl-card mdl-cell mdl-cell--12-col mdl-cell--12-col-phone" style="display: none">
                <div class="details"></div>
            </div>
            <?php
          //lendo os albuns
          $CDs = $Albums->getAlbuns();//pega todos os albuns do cantos específico
          foreach ($CDs as $cd){
          ?>
          <div class="show mdl-grid demo-blog__posts">
            <div class="mdl-card something-else mdl-cell mdl-cell--4-col mdl-cell--4-col-desktop" style="background-color: transparent">
                <img src="images/albums/<?= $cd['capa'] ?>"  style="width: 100%;"alt="">
            </div>
            <div class="mdl-card something-else mdl-cell mdl-cell--8-col mdl-cell--8-col-desktop">
              <div class="details">
                <h3><?= $cd['titulo'] ?></h3>
                  <b><?= $cd['ano_lancamento'] ?></b><br>
                <span><?= $cd['descricao'] ?></span>
              </div>
              <div class="mdl-card__actions mdl-card--border">
                <a href="controller/xml.php?action=album&id=<?= $cd['codigo']?>" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><i class="material-icons">code</i>XML</a>
                <?php
                    if ($App->isLogged()) {
                ?>
                        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect edit_album" data-idalbum="<?= $cd['codigo'] ?>"><i class="material-icons">edit</i></a>
                        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><i class="material-icons">delete</i></a>
                <?php
                    }
                ?>
              </div>
            </div>

          </div>
          <?php } ?>

        </div>
        <footer class="mdl-mini-footer">
          <div class="mdl-mini-footer--left-section">
            <button class="mdl-mini-footer--social-btn social-btn social-btn__twitter">
              <span class="visuallyhidden">Twitter</span>
            </button>
            <button class="mdl-mini-footer--social-btn social-btn social-btn__blogger">
              <span class="visuallyhidden">Facebook</span>
            </button>
            <button class="mdl-mini-footer--social-btn social-btn social-btn__gplus">
              <span class="visuallyhidden">Google Plus</span>
            </button>
          </div>
          <div class="mdl-mini-footer--right-section">
            <button class="mdl-mini-footer--social-btn social-btn__share">
              <i class="material-icons" role="presentation">share</i>
              <span class="visuallyhidden">share</span>
            </button>
          </div>
        </footer>
      </main>
      <div class="mdl-layout__obfuscator"></div>
    </div>
<?php
include "widgets/footer.php";
?>

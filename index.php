<?php
include_once "widgets/topo.php";
?>
<body>
    <div class="demo-blog mdl-layout mdl-js-layout has-drawer is-upgraded">
      <main class="mdl-layout__content">
        <div class="demo-blog__posts mdl-grid">
          <div class="mdl-card coffee-pic mdl-cell <?= ($App->isLogged()) ? "mdl-cell--8-col" : "mdl-cell--12-col" ?>">
            <div class="mdl-card__media mdl-color-text--grey-50">
            </div>
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
<!--              <div class="minilogo"></div>-->
              <?php include_once "widgets/pesquisa.php" ?>
<!--              <div>-->
<!--                <strong>The Newist</strong>-->
<!--                <span>2 days ago</span>-->
<!--              </div>-->
            </div>
          </div>
          <?php
            //para incluir o widget com as opções para o usuário logado
            include_once "widgets/loggedin.php";
            ?>
            <div class="searchResult mdl-card mdl-cell mdl-cell--12-col mdl-cell--12-col-phone" style="display: none">
                <div class="details"></div>
            </div>
            <?php
          //lendo os cantores
            $cantores = $Cantor->getCantores();
            foreach ($cantores as $cantor){
          ?>
          <div class="show mdl-card mdl-cell mdl-cell--6-col mdl-cell--12-col-phone">
            <div class="mdl-card__media mdl-color-text--grey-50" style="background-image: url('images/bandas/<?= $cantor['imagem'] ?>')">
              <h3 class="shadow"><a href="detalhecantor.php?id=<?= $cantor['codigo_cantor'] ?>"><?= $cantor['nome'] ?></h3>
            </div>
<!--            <div class="mdl-color-text--grey-600 mdl-card__supporting-text">-->
<!--              Enim labore aliqua consequat ut quis ad occaecat aliquip incididunt. Sunt nulla eu enim irure enim nostrud aliqua consectetur ad consectetur sunt ullamco officia. Ex officia laborum et consequat duis.-->
<!--            </div>-->
<!--            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">-->
<!--              <div class="minilogo"></div>-->
<!--              <div>-->
<!--                <strong>The Newist</strong>-->
<!--                <span>2 days ago</span>-->
<!--              </div>-->
<!--            </div>-->
            <div class="mdl-card__actions mdl-card--border">
              <a href="detalhecantor.php?id=<?= $cantor['codigo_cantor'] ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><i class="material-icons">sort</i> Info</a>
                <a href="controller/xml.php?action=cantor&idcantor=<?= $cantor['codigo_cantor']?>" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><i class="material-icons">code</i></a>
                <?php
                if ($App->isLogged()) {
                    ?>
<!--                    <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><i class="material-icons">edit</i></a>-->
<!--                    <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><i class="material-icons">delete</i></a>-->
                    <?php
                }
                ?>
            </div>
          </div>
          <?php } ?>
          <nav class="demo-nav mdl-cell mdl-cell--12-col">
            <div class="section-spacer"></div>
            <a href="<?= $App->getServer()."login.php" ?>" class="demo-nav__button" title="show more">
              Login
              <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                <i class="material-icons" role="presentation">arrow_forward</i>
              </button>
            </a>
          </nav>
        </div>
        <footer class="mdl-mini-footer">
          <div class="mdl-mini-footer--left-section">
            <button class="mdl-mini-footer--social-btn social-btn social-btn__twitter">
              <span class="visuallyhidden">Twitter</span>
            </button>
            <button class="mdl-mini-footer--social-btn social-btn social-btn__blogger">
              <span class="visuallyhidden">Facebook</span>
            </button>
            <button class="mdl-mini-footer--social-btn social-btn social-btn__gplus">
              <span class="visuallyhidden">Google Plus</span>
            </button>
          </div>
          <div class="mdl-mini-footer--right-section">
            <button class="mdl-mini-footer--social-btn social-btn__share">
              <i class="material-icons" role="presentation">share</i>
              <span class="visuallyhidden">share</span>
            </button>
          </div>
        </footer>
      </main>
      <div class="mdl-layout__obfuscator"></div>
    </div>
<?php
include "widgets/footer.php"
?>

<?php
include_once "conexao.php";
include_once "App.php";

class Login extends Gravadora{

    function __construct()
    {
        parent::__construct();
    }

    public function getUser($login, $senha){
        $query = $this->conn->prepare("SELECT * FROM usuario WHERE login = :login and senha = :senha");
        $query->execute(array(":login" => $login, ":senha" => sha1($senha)));
        return $query->fetch();
    }
}
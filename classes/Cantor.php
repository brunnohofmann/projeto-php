<?php
/**
 * Created by PhpStorm.
 * User: Brunno
 * Date: 08-Mar-16
 * Time: 1:44 PM
 */

include_once "App.php";

class Cantor extends Gravadora{

    function __construct()
    {
        parent::__construct();
    }

    public function getCantores(){
        $query = "SELECT * FROM cantor order by nome";
        $cantores = $this->conn->query($query);
        return $cantores->fetchAll();
    }

    public function getCantorById($idCantor){
        $query = $this->conn->prepare("SELECT * FROM cantor where codigo_cantor = :idcantor");
        $query->execute(array(":idcantor" => $idCantor));
        return $query->fetch();
    }

    public  function insertCantor($dados){
        $query = $this->conn->prepare("INSERT INTO cantor (nome, imagem) VALUES (:nome, :imagem)");
        $query->bindParam(':nome', $dados['cantor']);
        $query->bindParam(':imagem', $dados['imagem_cantor']);
        if ($query->execute()) return true;
    }
}
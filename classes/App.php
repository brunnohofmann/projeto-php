<?php

class Gravadora{

    public $conn;
    function __construct()
    {
        try {
            $dadosCon = file(Gravadora::getServer()."conexao/conexao.txt", FILE_IGNORE_NEW_LINES);
            $this->conn = new PDO('mysql:host=' . $dadosCon[0] . ';dbname=' . $dadosCon[1], $dadosCon[2], "");
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    function getServer(){
       return "http://".$_SERVER['SERVER_NAME']."/projetopoo/";
    }

    function isLogged(){
        if (isset($_SESSION['logado']) and  $_SESSION['logado'])
            return true;
    }

    function validaImagem($imagem){
        $regexValidator = "/\/(jpe?g|png)$/i";
        if (!preg_match($regexValidator, $imagem['type'])) throw new Exception("Tipo de arquivo Inválido");
            else if ($imagem['size'] > 400000) throw new Exception("Arquivo grande demais");
                else return true;
    }

}



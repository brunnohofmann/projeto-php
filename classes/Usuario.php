<?php
include_once "App.php";

class Usuario extends Gravadora{

    function __construct()
    {
        parent::__construct();
    }

    public  function insertUsuario($dados){
        $query = $this->conn->prepare("INSERT INTO usuario (nome, login, senha) VALUES (:nome, :login, :senha)");
        $query->bindParam(':nome', $dados['nome']);
        $query->bindParam(':login', $dados['login']);
        $query->bindParam(':senha', sha1($dados['senha']));
        if ($query->execute()) return true;
    }
}
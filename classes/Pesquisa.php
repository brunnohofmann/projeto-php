<?php
include_once "App.php";

class Pesquisa extends Gravadora{

    function __construct()
    {
        parent::__construct();
    }

    public function pesquisaCantores($nome){
        $query = $this->conn->prepare("SELECT * FROM cantor where nome like :nome");
        $query->execute(array(":nome" => "%$nome%"));
        return $query->fetchAll();
    }

    public function pesquisaAlbum($nome){
        $query = $this->conn->prepare("SELECT * FROM cd where titulo like :nome or ano_lancamento like :nome");
        $query->execute(array(":nome" => "%$nome%"));
        return $query->fetchAll();
    }
}
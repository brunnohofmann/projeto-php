<?php

trait BDconexao{

    public $conn;
    function __construct()
    {
        try {
            $dadosCon = file("conexao/conexao.txt", FILE_IGNORE_NEW_LINES);
            $this->conn = new PDO('mysql:host=' . $dadosCon[0] . ';dbname=' . $dadosCon[1], $dadosCon[2], "");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }
}
<?php
include_once "Cantor.php";

class AlbunsCantor extends Cantor{

    public $cantorId;
    function __construct($cantorId = null)//podemos saber o cantor quando se cria o objeto ou não
    {
        parent::__construct();
        $this->cantorId = $cantorId;
    }

    public function getAlbuns(){
        $query = $this->conn->prepare("SELECT * FROM cd where cantor_fk = :idcantor order by titulo ");
        $query->execute(array(":idcantor" => $this->cantorId));
        return $query->fetchAll();
    }

    public function getAlbumById($id){
        $query = $this->conn->prepare("SELECT * FROM cd join cantor on cd.cantor_fk = cantor.codigo_cantor  where codigo = :id ");
        $query->execute(array(":id" => $id));
        return $query->fetchAll();
    }


    public function insertAlbum($dados){
        $query = $this->conn->prepare("INSERT INTO cd (titulo, capa, ano_lancamento, descricao, cantor_fk) VALUES (:titulo, :capa, :ano, :descricao, :cantor)");
            $query->bindParam(':titulo', $dados['titulo_album']);
            $query->bindParam(':capa', $dados['capa']);
            $query->bindParam(':ano', $dados['ano_album']);
            $query->bindParam(':descricao', nl2br($dados['descricao_album']));
            $query->bindParam(':cantor', $dados['cantor']);

        if ($query->execute()) return true;
    }

    public function atualizaAlbum($dados, $id){
        $sql = (isset($dados['capa'])) ? "UPDATE cd SET titulo = :titulo, capa = :capa, ano_lancamento = :ano_lancamento, descricao = :descricao, cantor_fk = :cantor_fk WHERE codigo = :codigo" : "UPDATE cd set titulo = :titulo, ano_lancamento = :ano_lancamento, descricao = :descricao, cantor_fk = :cantor_fk WHERE codigo = :codigo";

        $query = $this->conn->prepare($sql);
        $query->bindParam(':titulo', $dados['titulo_album']);
        if (isset($dados['capa']))
            $query->bindParam(':capa', $dados['capa']);
        $query->bindParam(':ano_lancamento', $dados['ano_album']);
        $query->bindParam(':descricao', nl2br($dados['descricao_album']));
        $query->bindParam(':cantor_fk', $dados['cantor']);
        $query->bindParam(':codigo', $id);
        if ($query->execute()) return true;
    }
}
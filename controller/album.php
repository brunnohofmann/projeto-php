<?php
include_once "../classes/AlbunsCantor.php";

$Albuns = new AlbunsCantor();

$action=$_GET['action'];

switch($action){
    case "insertAlbum":
        $dadosAlbum = $_POST;
        if ($Albuns->validaImagem($_FILES['capa_album']))
            $dadosAlbum['capa'] =   md5(time()).strrchr($_FILES['capa_album']['name'], '.'); //criando nome do arquivo baseado na extensão

        if ($Albuns->insertAlbum($dadosAlbum)) {
            move_uploaded_file($_FILES['capa_album']['tmp_name'], "../images/albums/" . $dadosAlbum['capa']);
            header("Location: ".$Albuns->getServer()."detalhecantor.php?id=".$dadosAlbum['cantor']);
        }
    break;

    case "getInfo":
        $idAlbum = $_GET['id'];
        $response['album'] = $Albuns->getAlbumById($idAlbum);
        $response['cantores'] = $Albuns->getCantores();//para popular o select para alteração

        echo json_encode($response);
    break;

    case "editAlbum":
        $dadosAlbum = $_POST;
        $idAlbum = $_GET['id'];
        $infoAntiga = $Albuns->getAlbumById($idAlbum)[0];
        if (is_file($_FILES['capa_album']['tmp_name'])){//atualizando dados caso
            if ($Albuns->validaImagem($_FILES['capa_album']))
            $dadosAlbum['capa'] =   md5(time()).strrchr($_FILES['capa_album']['name'], '.'); //criando nome do arquivo baseado na extensão
            if ($Albuns->atualizaAlbum($dadosAlbum, $idAlbum))
            //deletando a imagem anterior
                unlink("../images/albums/".$infoAntiga['capa']);
            //salvando nova imagem
                move_uploaded_file($_FILES['capa_album']['tmp_name'], "../images/albums/" . $dadosAlbum['capa']);
        }
        if (!is_file($_FILES['capa_album']['tmp_name']))
                $Albuns->atualizaAlbum($dadosAlbum, $idAlbum);


        header("Location: ".$Albuns->getServer()."detalhecantor.php?id=".$dadosAlbum['cantor']);
    break;
}


?>
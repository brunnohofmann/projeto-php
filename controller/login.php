<?php
include_once "../classes/Login.php";
$action = $_GET['action'];
$Login = new Login();

//para a realização do Login
if ($action == "login"){
    $usuario = $Login->getUser($_POST['login'], $_POST['senha']);

    if($usuario){
        session_start();
        $_SESSION['logado'] = true;
        $_SESSION['usuario'] = $usuario;
        header("Location: ".$Login->getServer());
    }

    if (!$usuario){
        header("Location: ".$Login->getServer()."login.php");
    }
}

//para a realizaçao do logou
if ($action === "logout"){
    session_start();
    session_destroy();
    header("Location: ".$Login->getServer());
}
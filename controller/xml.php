<?php
header("Content-type: text/xml");
include_once "../classes/AlbunsCantor.php";

$idcantor = (isset($_GET['idcantor'])) ? $_GET['idcantor'] : "";
$Albuns = new AlbunsCantor($idcantor); //isso acontece pois a classe possue em seu construtor a indicação do cantor

$action=$_GET['action'];



switch($action){

    case "album":
        $idAlbum = $_GET['id'];
        $response = $Albuns->getAlbumById($idAlbum)[0];

        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->formatOutput = true;

        $root= $dom->createElement('album');
        $root = $dom->appendChild($root);

        $album = $dom->createElement('detalhealbum');
        $album = $root->appendChild($album);

        $nome = $dom->createElement('cantor', $response['nome']);
        $titulo = $dom->createElement('album', $response['titulo']);
        $ano = $dom->createElement('ano', $response['ano_lancamento']);
        $descricao = $dom->createElement('descricao', $response['descricao']);

        $nome = $album->appendChild($nome);
        $titulo = $album->appendChild($titulo);
        $ano = $album->appendChild($ano);
        $descricao= $album->appendChild($descricao);


        echo $dom->saveXML();

    break;
    case "cantor":
        //coletando dados
        $cantorinfo = $Albuns->getCantorById($idcantor);
        $albunsinfo = $Albuns->getAlbuns();

        //criando documento
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->formatOutput = true;

        //criando elemento pai
        $root= $dom->createElement('cantor');
        $root = $dom->appendChild($root);
        //adicionado tag ao elemento pai
        $cantor = $dom->createElement('detalhecantor');
        $cantor = $root->appendChild($cantor);
        //colocando as tags de detalhes do cantor
        $nome = $dom->createElement('nome', $cantorinfo['nome']);
        $nome = $cantor->appendChild($nome);

        //criandp a tag pai dos albums
        $albuns = $dom->createElement('albums');
        $albuns = $cantor->appendChild($albuns);

        //criando e insrindo album a album
        foreach($albunsinfo as $info){
            $album = $dom->createElement('album');
            $atributo = $dom->createAttribute("codigo");
            $atributo->value = $info['codigo'];
            $album->appendChild($atributo);

            $album = $albuns->appendChild($album);

            $titulo = $dom->createElement('titulo', $info['titulo']);
            $ano = $dom->createElement('ano', $info['ano_lancamento']);
            $descricao = $dom->createElement('descricao', $info['descricao']);

            $titulo = $album->appendChild($titulo);
            $ano = $album->appendChild($ano);
            $descricao = $album->appendChild($descricao);
        }
        echo $dom->saveXML();
    break;
}


?>